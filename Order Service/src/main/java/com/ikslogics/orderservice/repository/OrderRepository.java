package com.ikslogics.orderservice.repository;

import com.ikslogics.orderservice.entity.Orders;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Orders, String> {
}