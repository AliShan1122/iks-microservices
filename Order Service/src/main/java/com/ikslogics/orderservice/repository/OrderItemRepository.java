package com.ikslogics.orderservice.repository;

import com.ikslogics.orderservice.entity.OrderItems;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderItemRepository extends MongoRepository<OrderItems, String> {
}
