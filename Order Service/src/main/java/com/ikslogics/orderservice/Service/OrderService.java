package com.ikslogics.orderservice.Service;

import com.ikslogics.orderservice.entity.OrderItems;
import com.ikslogics.orderservice.entity.Orders;
import com.ikslogics.orderservice.mapper.OrderMapper;
import com.ikslogics.orderservice.repository.OrderItemRepository;
import com.ikslogics.orderservice.repository.OrderRepository;
import exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import models.Order;
import models.OrderItem;
import models.OrderItemInventoryDTO;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OrderService {
    private final OrderMapper orderMapper;
    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;

    public Order placeOrder(OrderItemInventoryDTO orderItem) {
        OrderItems orderItemEntity = orderMapper.getOrderItemEntity(orderItem);
        List<OrderItems> orderItemsList = new ArrayList<>();
        orderItemsList.add(orderItemEntity);
        List<OrderItems> savedOrderItems = orderItemRepository.saveAll(orderItemsList);
        Orders orders = new Orders();
        orders.setOrderItems(savedOrderItems);
        orders.setTotalPrice(orderItem.productCost().multiply(BigDecimal.valueOf(orderItem.quantity())));
        orders.setOrderDate(LocalDateTime.now());
        orders.setCustomerName(orderItem.customerName());
        orders.setOrderPlacedBy(orderItem.orderPlacedBy());
        Orders orderEntity = orderRepository.save(orders);
        List<OrderItem> orderItemList = List.of(new OrderItem(orderItem.productId(), orderItem.quantity(), orderItem.productCost()));
        return new Order(orderEntity.getId(), orderEntity.getCustomerName(), orderItemList, orderEntity.getTotalPrice(), orderEntity.getOrderDate(), orderEntity.getOrderPlacedBy());


    }

    public Order getOrder(String orderId) {
        Orders order = orderRepository.findById(orderId).orElseThrow(() -> new NotFoundException("Order Not Found"));
        var orderList = order.getOrderItems().stream().map(orderMapper::convertOrderItemEntityToDto).collect(Collectors.toList());
        return orderMapper.convertToOrderDto(order, orderList);

    }

    public List<Order> getAllOrder() {
        List<Orders> orders = orderRepository.findAll();
        if (orders.isEmpty()) {
            throw new NotFoundException("List of Order Not Found");
        }
        return orders.stream().map(orders1 ->
                orderMapper.convertToOrderDto(orders1, orders1.getOrderItems().stream()
                        .map(orderMapper::convertOrderItemEntityToDto).collect(Collectors.toList()))).collect(Collectors.toList());
    }
}
