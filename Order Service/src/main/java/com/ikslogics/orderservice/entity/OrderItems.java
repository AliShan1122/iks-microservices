package com.ikslogics.orderservice.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(collection = "order_items")
@Data
public class OrderItems {
    @Id
    private String id;
    private String productId;
    private int quantity;
    private BigDecimal price;
}
