package com.ikslogics.orderservice.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "orders")
@Data
public class Orders {
    @Id
    private String id;
    private LocalDateTime orderDate;
    private BigDecimal totalPrice;
    private String orderPlacedBy;
    private String customerName;
    @DBRef
    private List<OrderItems> orderItems = new ArrayList<>();
}
