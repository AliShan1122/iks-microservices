package com.ikslogics.orderservice.controller;

import com.ikslogics.orderservice.Service.OrderService;
import lombok.AllArgsConstructor;
import models.Order;
import models.OrderItem;
import models.OrderItemInventoryDTO;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class OrderController implements controllers.OrderController {
    private final OrderService orderService;
    @Override
    public Order placeOrder(OrderItemInventoryDTO orderItem) {
        return orderService.placeOrder(orderItem);
    }

    @Override
    public Order getOrder(String orderId) {
        return orderService.getOrder(orderId);
    }

    @Override
    public List<Order> getAllOrder() {
        return orderService.getAllOrder();
    }
}
