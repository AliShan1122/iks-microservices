package com.ikslogics.orderservice.mapper;

import com.ikslogics.orderservice.entity.OrderItems;
import com.ikslogics.orderservice.entity.Orders;
import models.Order;
import models.OrderItem;
import models.OrderItemInventoryDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderMapper {
    public OrderItems getOrderItemEntity(OrderItemInventoryDTO orderItem) {
        OrderItems orderItems=new OrderItems();
        orderItems.setQuantity(orderItem.quantity());
        orderItems.setProductId(String.valueOf(orderItem.productId()));
        orderItems.setPrice(orderItem.productCost());
        return orderItems;
    }

    public Order convertToOrderDto(Orders orderEntity, List<OrderItem> orderItemList) {

        return new Order(orderEntity.getId(),orderEntity.getCustomerName(),orderItemList,orderEntity.getTotalPrice(),orderEntity.getOrderDate(),orderEntity.getOrderPlacedBy());
    }
    public OrderItem convertOrderItemEntityToDto(OrderItems orderItems){
        return new OrderItem(Long.valueOf(orderItems.getProductId()),orderItems.getQuantity(),orderItems.getPrice());
    }
}
