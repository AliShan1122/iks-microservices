package authenticationservice.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService service;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(@RequestBody RegisterRequest request) {
        return ResponseEntity.ok(service.register(request));
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request) throws Throwable {
        return ResponseEntity.ok(service.authenticate(request));
    }

    @ControllerAdvice
    public class GlobalExceptionHandler {
        @ExceptionHandler(Exception.class)
        public ResponseEntity<?> handleException(Exception ex) {
          HashMap<String,String> stringStringMap=new HashMap<>() ;
          stringStringMap.put("Error Message",ex.getMessage());
          stringStringMap.put("", ex.getLocalizedMessage());
            return ResponseEntity.status(500).contentType(MediaType.APPLICATION_JSON).body(stringStringMap);
        }
    }

}
