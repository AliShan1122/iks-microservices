package authenticationservice.user;

public enum Role {

  USER,
  ADMIN,
  SUPER_ADMIN
}
