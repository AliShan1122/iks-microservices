package com.ikslogics.productservice.controller;

import com.ikslogics.productservice.Service.ProductService;
import lombok.RequiredArgsConstructor;
import models.Inventory;
import models.Product;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ProductController implements controllers.ProductController {
    private final ProductService productService;
    @Override
    public Product addProduct(Product body) {
        return productService.addProduct(body);
    }

    @Override
    public Product getProduct(Long productId) {
        return productService.getProduct(productId);
    }

    @Override
    public void deleteProduct(Long productId) {
         productService.deleteProduct(productId);
    }

    @Override
    public Inventory getInventory(Long productId) {
       return productService.getInventory(productId);
    }

    @Override
    public List<Product> getAllProduct() {
        return productService.getAllProduct();
    }
}
