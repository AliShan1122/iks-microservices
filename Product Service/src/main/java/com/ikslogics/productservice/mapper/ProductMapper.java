package com.ikslogics.productservice.mapper;

import com.ikslogics.productservice.entity.InventoryEntity;
import com.ikslogics.productservice.entity.ProductEntity;
import models.Inventory;
import models.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductMapper {
    public ProductEntity convertToProductEntity(Product body) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(body.price());
        productEntity.setDescription(body.description());
        productEntity.setName(body.name());
        productEntity.setSupplierName(body.supplierName());
        return productEntity;
    }

    public Product convertToProduct(ProductEntity product, int quantity) {
        return new Product(product.getProductId(), product.getName(), product.getDescription(), product.getPrice(), product.getSupplierName(), quantity);
    }

    public Inventory convertToInventoryDto(InventoryEntity inventoryEntity) {
        return new Inventory(inventoryEntity.getProduct().getProductId(), inventoryEntity.getQuantity());
    }

    public List<Product> mapProductEntitiesToProducts(List<ProductEntity> productEntities) {
        List<Product> products = new ArrayList<>();
        for (ProductEntity productEntity : productEntities) {
            Product product = new Product(productEntity.getProductId(), productEntity.getName(), productEntity.getDescription(), productEntity.getPrice(), productEntity.getSupplierName(), 0);
            products.add(product);
        }
        return products;
    }

}
