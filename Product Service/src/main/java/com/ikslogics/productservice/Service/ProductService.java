package com.ikslogics.productservice.Service;

import com.ikslogics.productservice.Repository.InventoryRepository;
import com.ikslogics.productservice.Repository.ProductRepository;
import com.ikslogics.productservice.entity.InventoryEntity;
import com.ikslogics.productservice.entity.ProductEntity;
import com.ikslogics.productservice.mapper.ProductMapper;
import exceptions.BadRequestException;
import exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import models.Inventory;
import models.Product;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final InventoryRepository inventoryRepository;

    public Product addProduct(Product body) {
        if (Objects.isNull(body)) {
            throw new BadRequestException("Product can't be null or empty");
        }
        ProductEntity productEntity = productMapper.convertToProductEntity(body);
        InventoryEntity inventoryEntity = new InventoryEntity();
        inventoryEntity.setProduct(productEntity);
        inventoryEntity.setQuantity(body.quantity() == 0 ? 1 : body.quantity());
        productEntity.getInventories().add(inventoryEntity);
        ProductEntity product = productRepository.save(productEntity);
        return productMapper.convertToProduct(product, body.quantity());
    }

    public Inventory getInventory(Long productId) {
        ProductEntity productEntity = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Product not found"));
        InventoryEntity inventoryEntity = inventoryRepository.findByProduct(productEntity);
        return productMapper.convertToInventoryDto(inventoryEntity);
    }

    public Product getProduct(Long productId) {
        ProductEntity productEntity = productRepository.findById(productId).orElseThrow(() -> new NotFoundException("Product Not Found"));
        return productMapper.convertToProduct(productEntity, 0);
    }

    public void deleteProduct(Long productId) {
        ProductEntity product = productRepository.findById(productId).orElseThrow(() -> new NotFoundException("Product Not Found to Delete"));
        productRepository.delete(product);
    }

    public List<Product> getAllProduct() {
        List<ProductEntity> products = productRepository.findAll();
        if (products.isEmpty()) {
            throw new NotFoundException("Products Not Found");
        }
        return productMapper.mapProductEntitiesToProducts(products);
    }
}
