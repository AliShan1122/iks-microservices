package com.ikslogics.productservice.Repository;

import com.ikslogics.productservice.entity.InventoryEntity;
import com.ikslogics.productservice.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface InventoryRepository extends JpaRepository<InventoryEntity,Long> {
    InventoryEntity findByProduct(ProductEntity productEntity);
@Transactional
@Modifying
@Query("delete from InventoryEntity i where i.product = ?1")
void deleteByProductOrId(Long productId);
}
