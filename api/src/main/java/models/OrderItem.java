package models;

import java.math.BigDecimal;
import java.util.Comparator;

public record OrderItem(Long productId, int quantity, BigDecimal productCost) implements Comparable<OrderItem> {


    @Override
    public int compareTo(OrderItem o) {
        return (int) (this.productId-o.productId);
    }
}
