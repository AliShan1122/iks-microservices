package models;

public record LoginRequest(String userName,String password) {
}
