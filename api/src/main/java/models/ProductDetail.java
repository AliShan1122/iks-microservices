package models;

public record ProductDetail(String productID,String remainingItems) {
}
