package models;

import java.util.List;

public record LoginResponse(String Jwt, List<String> role) {
}
