package models;

import java.math.BigDecimal;

public record Product(Long productId,String name, String description, BigDecimal price,String supplierName,int quantity) {
}
