package models;

public record RegisterUser(Long id, String name, String email, String password, String role, Boolean isAdmin, Boolean isSuperAdmin) {
}
