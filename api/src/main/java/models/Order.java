package models;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public record Order(String OrderId,String customerName, List<OrderItem> orderItems, BigDecimal totalCost, LocalDateTime orderDate,String OrderPlacedBy) {
}
