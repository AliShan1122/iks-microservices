package models;

import java.math.BigDecimal;

public record OrderItemInventoryDTO(Long productId, int quantity, BigDecimal productCost,String orderPlacedBy,String customerName) {
}
