package controllers;

import models.LoginRequest;
import models.LoginResponse;
import models.RegisterUser;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/v1/auth")
public interface AuthenticationController {
    @PostMapping(
            value    = "/login",
            consumes = "application/json",
            produces = "application/json")
    LoginResponse login(@RequestBody LoginRequest loginRequest);
    @PostMapping(
            value    = "/register",
            consumes = "application/json",
            produces = "application/json")
    RegisterUser register(@RequestBody RegisterUser registerUser);
}
