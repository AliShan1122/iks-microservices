package controllers;

import models.Inventory;
import models.Product;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface ProductController {
    @PostMapping(value = "/product", consumes = "application/json", produces = "application/json")
    Product addProduct(@RequestBody Product body);

    @GetMapping(value = "/product/{productId}", produces = "application/json")
    Product getProduct(@PathVariable Long productId);

    @DeleteMapping(value = "/product/{productId}")
    void deleteProduct(@PathVariable Long productId);

    @GetMapping(value = "/product/inventory/{productId}", produces = "application/json")
    Inventory getInventory(@PathVariable Long productId);

    @GetMapping(value = "/product/getAllProducts", produces = "application/json")
    List<Product> getAllProduct();
}
