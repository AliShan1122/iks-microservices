package controllers;
import jakarta.servlet.http.HttpServletRequest;
import models.Order;
import models.OrderItem;
import models.Product;
import models.ProductDetail;
import org.springframework.web.bind.annotation.*;
import java.util.List;

public interface OrderProductCompositeController {
    @PostMapping(value = "/inventory-management/placeOrder", produces = "application/json", consumes = "application/json")
    Order placeOrder(@RequestBody OrderItem orderItem, HttpServletRequest httpServletRequest);

    @GetMapping(value = "/inventory-management/getProductDetail/{productId}", produces = "application/json")
    ProductDetail getProductDetail(@PathVariable int productId, HttpServletRequest httpServletRequest);

    @PostMapping(value = "inventory-management/addProduct", consumes = "application/json", produces = "application/json")
    Product addProduct(@RequestBody Product product, HttpServletRequest httpServletRequest);

    @DeleteMapping(value = "/inventory-management/product/{productId}")
    void deleteProduct(@PathVariable Long productId, HttpServletRequest httpServletRequest);

    @GetMapping(value = "/inventory-management/product/getAllProducts", produces = "application/json")
    List<Product> getAllProduct(HttpServletRequest httpServletRequest);

    @GetMapping(value = "/inventory-management/order/getAll", produces = "application/json")
    List<Order> getAllOrder(HttpServletRequest httpServletRequest);

    @GetMapping(value = "/order/{orderId}", produces = "application/json")
    Order getOrder(@PathVariable String orderId);
}
