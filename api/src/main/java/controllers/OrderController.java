package controllers;

import models.Order;
import models.OrderItem;
import models.OrderItemInventoryDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface OrderController {
    @PostMapping(value = "/order", produces = "application/json", consumes = "application/json")
    Order placeOrder(@RequestBody OrderItemInventoryDTO orderItem);

    @GetMapping(value = "/order/{orderId}", produces = "application/json")
    Order getOrder(@PathVariable String orderId);

    @GetMapping(value = "/order/getAll", produces = "application/json")
    List<Order> getAllOrder();

}
