package com.ikslogics.inventorymanagement.controller;

import com.ikslogics.inventorymanagement.service.InventoryService;
import controllers.OrderProductCompositeController;
import exceptions.UnauthorizedException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import models.Order;
import models.OrderItem;
import models.Product;
import models.ProductDetail;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class InventoryController implements OrderProductCompositeController {
    private final InventoryService inventoryService;

    @Override
    public Order placeOrder(OrderItem orderItem, HttpServletRequest httpServletRequest) {
        return inventoryService.createOrder(orderItem, httpServletRequest);
    }

    @Override
    public ProductDetail getProductDetail(int productId, HttpServletRequest httpServletRequest) {
        if (!httpServletRequest.getAttribute("Role").equals("ADMIN") && !httpServletRequest.getAttribute("Role").equals("SUPER_ADMIN")) {
            throw new UnauthorizedException("Unauthorized");
        }
        return inventoryService.getProductDetail(productId);
    }

    @Override
    public Product addProduct(Product product, HttpServletRequest httpServletRequest) {
        if (!httpServletRequest.getAttribute("Role").equals("ADMIN") && !httpServletRequest.getAttribute("Role").equals("SUPER_ADMIN")) {
            throw new UnauthorizedException("Unauthorized");
        }
        return inventoryService.addProduct(product);
    }

    @Override
    public void deleteProduct(Long productId, HttpServletRequest httpServletRequest) {
        if (!httpServletRequest.getAttribute("Role").equals("SUPER_ADMIN")) {
            throw new UnauthorizedException("Unauthorized");
        }
        inventoryService.deleteProduct(productId);
    }

    @Override
    public List<Product> getAllProduct(HttpServletRequest httpServletRequest) {
        if (!httpServletRequest.getAttribute("Role").equals("ADMIN") && !httpServletRequest.getAttribute("Role").equals("SUPER_ADMIN")) {
            throw new UnauthorizedException("Unauthorized");
        }
        return inventoryService.getAllProducts();
    }

    @Override
    public List<Order> getAllOrder(HttpServletRequest httpServletRequest) {
        if (!httpServletRequest.getAttribute("Role").equals("ADMIN") && !httpServletRequest.getAttribute("Role").equals("SUPER_ADMIN")) {
            throw new UnauthorizedException("Unauthorized");
        }
        return inventoryService.getAllOrder();
    }

    @Override
    public Order getOrder(String orderId) {
        return inventoryService.getOrder(orderId);
    }
}
