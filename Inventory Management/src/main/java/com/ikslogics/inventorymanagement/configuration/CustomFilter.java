package com.ikslogics.inventorymanagement.configuration;

import com.ikslogics.inventorymanagement.service.JwtService;
import exceptions.BadRequestException;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class CustomFilter implements Filter {
    private final JwtService jwtService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String authorizationHeader = httpRequest.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String jwtToken = authorizationHeader.substring(7);
            try {
                if (!jwtService.isTokenExpired(jwtToken)) {
                    String userName = jwtService.extractUsername(jwtToken);
                    request.setAttribute("user", userName);
                    var claims = jwtService.extractAllClaims(jwtToken);
                    request.setAttribute("Role", claims.get("Role"));
                }
            } catch (ExpiredJwtException jwt) {
                throw new BadRequestException("Invalid Token or Token Expired");
            }
        }
        chain.doFilter(request, response);
    }

    // implement the init() and destroy() methods as needed
}
