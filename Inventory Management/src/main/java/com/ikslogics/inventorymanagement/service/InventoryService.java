package com.ikslogics.inventorymanagement.service;

import exceptions.BadRequestException;
import exceptions.NotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Service
public class InventoryService {
    private final Logger logger = LoggerFactory.getLogger(InventoryService.class);
    private final String productBaseUrl;
    private final String OrderBaseUrl;
    private final RestTemplate restTemplate;

    public InventoryService(@Value("${porduct.service.url}") String productBaseUrl, @Value("${order.service.url}") String orderBaseUrl, RestTemplate restTemplate) {
        this.productBaseUrl = productBaseUrl;
        this.OrderBaseUrl = orderBaseUrl;
        this.restTemplate = restTemplate;
    }

    public Order createOrder(OrderItem orderItem, HttpServletRequest httpServletRequest) {
        logger.info(productBaseUrl + orderItem.productId());
        Order order = null;
        String userName = (String) httpServletRequest.getAttribute("user");
        BigDecimal totalPrice;
        try {
            Product product = restTemplate.getForObject(productBaseUrl + "/" + orderItem.productId(), Product.class);

            if (Objects.isNull(product)) {
                throw new NotFoundException("Product Not Found");
            }
            logger.info("product", product);
            totalPrice = product.price().multiply(BigDecimal.valueOf(orderItem.quantity()));
            logger.info(String.valueOf(totalPrice));
            OrderItemInventoryDTO orderItemInventoryDTO=new OrderItemInventoryDTO(product.productId(),orderItem.quantity(),product.price(),userName,userName);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<OrderItemInventoryDTO> request = new HttpEntity<>(orderItemInventoryDTO, headers);
            order = restTemplate.postForObject(OrderBaseUrl, request, Order.class);
            logger.info("order", order);

        } catch (Exception ex) {
            throw new BadRequestException(ex.getMessage());
        }
        return new Order(order.OrderId(), userName, order.orderItems(), totalPrice, order.orderDate(), userName);
    }

    public ProductDetail getProductDetail(int productId) {
        ProductDetail productDetail;
        try {
            Inventory inventory = restTemplate.getForObject(productBaseUrl + "/inventory/" + +productId, Inventory.class);
            if (Objects.isNull(inventory)) {
                throw new NotFoundException("Can't get Product Detail");
            }
            logger.info("inventory getProductDetail", inventory);
            productDetail = new ProductDetail(String.valueOf(inventory.productId()), String.valueOf(inventory.quantity()));
        } catch (Exception ex) {
            throw new BadRequestException("Unable to process Request" + ex);
        }
        return productDetail;
    }

    public Product addProduct(Product product) {
        Product responseProduct = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Product> request = new HttpEntity<>(product, headers);
            responseProduct = restTemplate.postForObject(productBaseUrl, request, Product.class);
            logger.info("Add Product", responseProduct);
        } catch (Exception e) {
            throw new BadRequestException("Having issues while adding products");
        }
        return responseProduct;
    }

    public void deleteProduct(Long productId) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> requestEntity = new HttpEntity<>(headers);
            ResponseEntity<String> responseEntity = restTemplate.exchange(productBaseUrl + "/" + productId, HttpMethod.DELETE, requestEntity, String.class);
        } catch (Exception e) {
            throw new BadRequestException("Unable to Delete Product");
        }
    }

    public List<Product> getAllProducts() {
        List<Product> products;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> requestEntity = new HttpEntity<>(headers);
            ResponseEntity<List<Product>> responseEntity = restTemplate.exchange(productBaseUrl + "/getAllProducts", HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Product>>() {
            });
            products = responseEntity.getBody();
            if (products.isEmpty()) {
                throw new NotFoundException("Products Not Found");
            }
        } catch (Exception ex) {
            throw new BadRequestException("Facing issue while retrieving product list");
        }
        return products;
    }

    public List<Order> getAllOrder() {
        List<Order> Orders;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> requestEntity = new HttpEntity<>(headers);
            ResponseEntity<List<Order>> responseEntity = restTemplate.exchange(OrderBaseUrl + "/getAll", HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Order>>() {
            });
            Orders = responseEntity.getBody();
            if (Orders.isEmpty()) {
                throw new NotFoundException("Orders Not Found");
            }
        } catch (Exception ex) {
            throw new BadRequestException("Facing issue while retrieving Order list");
        }
        return Orders;
    }

    public Order getOrder(String orderId) {
        Order orderResponse;
        try {
            orderResponse = restTemplate.getForObject(OrderBaseUrl + "/" + orderId, Order.class);
            if (Objects.isNull(orderResponse)) {
                throw new NotFoundException("Order Not Found");
            }
        } catch (Exception ex) {
            throw new BadRequestException("Unable to process Request" + ex);
        }
        return orderResponse;
    }
}
